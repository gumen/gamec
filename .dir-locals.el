((c-mode . ((c-file-style . "linux")
	    (eval . (smart-tabs-insinuate 'c))
	    (truncate-lines . nil)
	    (whitespace-style . (face tabs spaces trailing space-before-tab indentation empty space-after-tab space-mark tab-mark missing-newline-at-eof)))))
