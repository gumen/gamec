#include <stdio.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_timer.h>
#include <SDL2/SDL_image.h>

#define IMG_PATH "assets/cat.png"
#define SPEED 300 /* Speed in pixels per seconds*/

typedef struct {
	char *title;
	int position_x;
	int position_y;
	Uint16 width;
	Uint16 height;
} Window;

typedef struct {
	SDL_Window *window;
	SDL_Renderer *renderer;
} Game;

SDL_Window *
create_centered_window(const Window *window)
{
	SDL_Window *sdl_window = SDL_CreateWindow(window->title,
	                                          window->position_x,
	                                          window->position_y,
	                                          window->width,
	                                          window->height,
	                                          0);

	if (!sdl_window) {
		printf("error creating window: %s\n", SDL_GetError());
		SDL_Quit();
		exit(1);
	}

	return sdl_window;
}

SDL_Renderer *
create_renderer(const Game *game)
{
	/* create a renderer, which sets up the graphics hardware */
	Uint32 flags = SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC;
	SDL_Renderer *renderer = SDL_CreateRenderer(game->window, -1, flags);

	if (!renderer) {
		printf("error creating renderer: %s\n", SDL_GetError());
		SDL_DestroyWindow(game->window);
		SDL_Quit();
		exit(1);
	}

	return renderer;
}

SDL_Surface *
create_image_surface(const Game *game, const char *path)
{
	SDL_Surface *surface = IMG_Load(path);

	if (!surface) {
		printf("error creating surface\n");
		SDL_DestroyRenderer(game->renderer);
		SDL_DestroyWindow(game->window);
		SDL_Quit();
		exit(1);
	}

	return surface;
}

SDL_Texture *
create_texture_from_image(const Game *game, const char *path)
{
	SDL_Surface *surface = create_image_surface(game, path);

	/* Load image data into graphic's hardware memory */
	SDL_Texture *texture = SDL_CreateTextureFromSurface(game->renderer,
	                                                    surface);
	SDL_FreeSurface(surface);
	if (!texture) {
		printf("error creating texture %s\n", SDL_GetError());
		SDL_DestroyRenderer(game->renderer);
		SDL_DestroyWindow(game->window);
		SDL_Quit();
		exit(1);
	}

	return texture;
}

int
main(void)
{
	/* attempt to initialize graphics system */
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		printf("error initializing SDL: %s\n", SDL_GetError());
		return 1;
	}

	Window window = {
		.title = "Hello There",
		.position_x = SDL_WINDOWPOS_CENTERED,
		.position_y = SDL_WINDOWPOS_CENTERED,
		.width = 640,
		.height = 480,
	};

	Game game = {
		.window = create_centered_window(&window),
		.renderer = create_renderer(&game),
	};

	SDL_Texture *texture = create_texture_from_image(&game, IMG_PATH);

	/* struct to hold the position and size of the sprite */
	SDL_Rect rectangle;

	/* get ans scale the dimensions of texture */
	SDL_QueryTexture(texture, NULL, NULL, &rectangle.w, &rectangle.h);
	rectangle.w /= 4;
	rectangle.h /= 4;

	/* Start sprite at the center of screen */
	float position_x = (window.width  - rectangle.w) / 2;
	float position_y = (window.height - rectangle.h) / 2;
	float velocity_x = 0;
	float velocity_y = 0;

	/* set to 1 when window close button is pressed */
	int close_requested = 0;

	/* animation loop */
	while (!close_requested) {
		/* process events */
		SDL_Event event;
		while (SDL_PollEvent(&event))
			/* If X button in top right (or left) window
			 * corner is clicked OR if "q" key is pressed
			 * on keyboard */
			if (event.type == SDL_QUIT ||
			    (event.type == SDL_KEYDOWN &&
			     event.key.keysym.scancode == SDL_SCANCODE_Q))
				close_requested = 1;

		/* get cursor positing relative to window */
		int mouse_x, mouse_y;
		int buttons = SDL_GetMouseState(&mouse_x, &mouse_y);

		/* determine velocity */
		int target_x = mouse_x - rectangle.w / 2;
		int target_y = mouse_y - rectangle.h / 2;
		float delta_x = target_x - position_x;
		float delta_y = target_y - position_y;
		float distance = sqrt(delta_x * delta_x + delta_y * delta_y);

		/* prevent jitter */
		if (distance < 5) {
			velocity_x = 0;
			velocity_y = 0;
		} else {
			velocity_x = delta_x * SPEED / distance;
			velocity_y = delta_y * SPEED / distance;
		}

		/* reverse velocity if mouse button 1 pressed */
		if (buttons & SDL_BUTTON(SDL_BUTTON_LEFT)) {
			velocity_x = -velocity_x;
			velocity_y = -velocity_y;
		}

		/* update position */
		position_x += velocity_x / 60;
		position_y += velocity_y / 60;

		/* set the y position to the struct */
		rectangle.x = (int) position_x;
		rectangle.y = (int) position_y;

		/* collision detection */
		if (position_x <= 0)
			position_x = 0;
		if (position_y <= 0)
			position_y = 0;
		if (position_x >= window.width - rectangle.w)
			position_x = window.width - rectangle.w;
		if (position_y >= window.height - rectangle.h)
			position_y = window.height - rectangle.h;

		/* clear the window */
		SDL_RenderClear(game.renderer);

		/* draw the image to the window */
		SDL_RenderCopy(game.renderer, texture, NULL, &rectangle);
		SDL_RenderPresent(game.renderer);

		/* wait 1/60th of a second */
		SDL_Delay(1000/60);
	}

	SDL_DestroyTexture(texture);
	SDL_DestroyRenderer(game.renderer);
	SDL_DestroyWindow(game.window);
	SDL_Quit();

	return 0;
}
