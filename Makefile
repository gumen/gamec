include config.mk

all: gamec

main.o: main.c

gamec: main.o
	$(CC) -o $@ $^ $(SDLFLAGS) $(CFLAGS)

clean:
	$(RM) gamec
	$(RM) main.o
